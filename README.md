SouR (Souvenir Recommender)
Implement a Web application for people who make voyages including multiple regions / countries
to manage and recommend souvenirs for each visited territory (eg, those who have gone through
Australia can suggest buying a boomerang or a T-shirt printed with silhouette of a kangaroo 
for relatives at home). The recommendations can also be made according to local culture
and / or customs, the time of the visit (for example, those who traveled to Romania
in February-March can indicate the martisoare) and the profile of the people who
will benefit from souvenirs: family, friends, acquaintances, school / service colleagues 
(from the nephew who has just returned from Paris, her grandmother would be delighted to
receive a porcelain bibel displaying the Eiffel Tower or a reproduction of the Mona Lisa
painting, while the child of his best friend would prefer a technological toy around the
Palais de la découverte). The list of recommended souvenirs will be available in HTML, 
CSV, JSON and XML formats. Bonus: map the points of interest where you can buy souvenirs.