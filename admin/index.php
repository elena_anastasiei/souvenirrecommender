<?php
session_start();
$host     = 'localhost';
$dbname   = 'sour';
$username = 'root';
$password = '';

try {
    $databaseConnection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOEception $e) {
    echo $e->getMessage();
}
if (isset($_POST['submit'])) {
    $errMsg   = '';
    //username and password sent from Form
    $username = $_POST['username'];
    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    
    if ($username == '')
        $errMsg .= 'You must enter your Username<br>';
    
    if ($password == '')
        $errMsg .= 'You must enter your Password<br>';
    
    
    if ($errMsg == '') {
        $records = $databaseConnection->prepare('SELECT id,user_name,user_pass FROM  admin WHERE user_name = :username');
        $records->bindParam(':username', $username);
        $records->execute();
        $results = $records->fetch(PDO::FETCH_ASSOC);
        
        if (count($results) > 0 && password_verify($password, $results['user_pass'])) {
            $_SESSION['username'] = $results['user_name'];
            $url                  = 'admin.php';
            header("Location: $url");
            exit;
        } else {
            $errMsg .= 'Username and Password are not found<br>';
        }
        
    }
}
if(isset($_GET['logout'])){
    $session_destroy();
}
   
?>
<html>
   <head>
      <title>Login Page PHP Script</title>
     <link rel="stylesheet" type="text/css" href="admin/css/login.css">
   </head>
   <body>
      <div align="center">
         <div class="border_box login_div">
            <?php
               if(isset($errMsg)){
               	echo '<div class="error_msg">'.$errMsg.'</div>';
               }
               ?>
            <div class="login_f border_box"><b>Admin Login</b></div>
            <div class="login_d"">
               <form action="" method="post">
                  <label>Username: </label><input type="text" name="username" class="border_box"/><br /><br />
                  <label>Password: </label><input type="password" name="password" class="border_box" /><br/><br />
                  <input type="submit" name='submit' value="SUBMIT" class="submit border_box" style="color: #ecf0f1;"/><br />
               </form>
            </div>
         </div>
      </div>
   </body>
</html>