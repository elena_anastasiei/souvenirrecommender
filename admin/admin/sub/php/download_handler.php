
<?php

function utf8ize($d)
	{
	if (is_array($d))
		{
		foreach($d as $k => $v)
			{
			$d[$k] = utf8ize($v);
			}
		}
	  else
	if (is_string($d))
		{
		return utf8_encode($d);
		}

	return $d;
	}

$host = 'localhost';
$dbname = 'sour';
$username = 'root';
$password = '';
try
	{
	$databaseConnection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
	$databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

catch(PDOEception $e)
	{
	echo $e->getMessage();
	}

$statement = $databaseConnection->query('SELECT * FROM souvenir');
$statement->setFetchMode(PDO::FETCH_ASSOC);

if (isset($_POST['d_xml']))
	{
	$xml = new XMLWriter();
	$xml->openURI("php://output");
	$xml->startDocument("1.0");
	$xml->setIndent(true);
	$xml->startElement('souvenirs');
	foreach($statement as $row)
		{
		$xml->startElement("souvenir");
		$xml->writeAttribute('id', $row['ID']);
		$xml->startElement("id");
		$xml->writeRaw($row['ID']);
		$xml->endElement();
		$xml->startElement("name");
		$xml->writeRaw($row['NAME']);
		$xml->endElement();
		$xml->startElement("description");
		$xml->writeRaw($row['DESCRIPTION']);
		$xml->endElement();
		$xml->startElement("gender");
		$xml->writeRaw($row['GENDER']);
		$xml->endElement();
		$xml->startElement("age");
		$xml->writeRaw($row['AGE']);
		$xml->endElement();
		$xml->startElement("relation");
		$xml->writeRaw($row['RELATION']);
		$xml->endElement();
		$xml->startElement("availability");
		$xml->startElement("periodfrom");
		$xml->writeRaw($row['PERIODFROM']);
		$xml->endElement();
		$xml->startElement("periodto");
		$xml->writeRaw($row['PERIODTO']);
		$xml->endElement();
		$xml->endElement();
		$xml->startElement("adress");
		$xml->writeRaw($row['ADRESS']);
		$xml->endElement();
		$xml->endElement();
		}

	$xml->endElement();
	$xml->endDocument();
	header('Content-type: text/xml');
	header('Content-Disposition: attachment; filename=souvenirs.xml');
	$xml->flush();
	}

if (isset($_POST['d_json']))
	{;
	$souvenir_data = array();
	foreach($statement as $row)
		{
		$id = $row['ID'];
		$name = $row['NAME'];
		$description = $row['DESCRIPTION'];
		$gender = $row['GENDER'];
		$age = $row['AGE'];
		$relation = $row['RELATION'];
		$periodfrom = $row['PERIODFROM'];
		$periodto = $row['PERIODTO'];
		$adress = $row['ADRESS'];
		array_push($souvenir_data, array(
			'id' => $id,
			'name' => $name,
			'description' => $description,
			'gender' => $gender,
			'age' => $age,
			'relation' => $relation,
			'periodfrom' => $periodfrom,
			'periodto' => $periodto,
			'adress' => $adress
		));
		}

	$json_data = json_encode(utf8ize($souvenir_data));
	$fp = fopen('php://output', 'w');
	if ($fp)
		{
		header('Content-Type: application/json');
		header('Content-Disposition: attachment; filename=souvenirs.json');
		fwrite($fp, $json_data);
		}
	}

if (isset($_POST['d_html']))
	{
	echo 'Finished downloading "souvenirs.html" file.';
	}

if (isset($_POST['d_csv']))
	{
	$fp = fopen('php://output', 'w');
	if ($fp)
		{
		header("Content-Type: text/csv");
		header("Content-disposition: attachment; filename=souvenirs.csv");
		foreach($statement as $row)
			{
			fputcsv($fp, array_values($row));
			}
		}
	}

?>