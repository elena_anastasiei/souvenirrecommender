<?php

/**
 * PHP MySQL Create Table Sour
 */
class CreateTableSour {

    /**
     * database host
     */
    const DB_HOST = 'localhost';

    /**
     * database name
     */
    const DB_NAME = 'sour';

    /**
     * database user
     */
    const DB_USER = 'root';
    /*
     * database password
     */
    const DB_PASSWORD = '';

    /**
     *
     * @var type 
     */
    private $pdo = null;

    /**
     * Open the database connection
     */
    public function __construct() {
        // open database connection
        $conStr = sprintf("mysql:host=%s;dbname=%s", self::DB_HOST, self::DB_NAME);
        try {
            $this->pdo = new PDO($conStr, self::DB_USER, self::DB_PASSWORD);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * close the database connection
     */
    public function __destruct() {
        // close the database connection
        $this->pdo = null;
    }

    /**
     * create the tasks table
     * @return boolean returns true on success or false on failure
     */
  public function createTaskTable() {
        $sql = <<<EOSQL
            CREATE TABLE IF NOT EXISTS souvenir (
                ID     INT AUTO_INCREMENT PRIMARY KEY,
                NAME     VARCHAR (50)        DEFAULT NULL,
				DESCRIPTION VARCHAR (255)	DEFAULT NULL,
				IMG LONGBLOB DEFAULT NULL,
				GENDER VARCHAR (50) DEFAULT NULL,
                AGE VARCHAR (200)	DEFAULT NULL,
                RELATION VARCHAR (400)	DEFAULT NULL,
                PERIODFROM  DATE                 DEFAULT NULL,
				PERIODTO  DATE                 DEFAULT NULL,
                ADRESS VARCHAR (255)	DEFAULT NULL
            );
EOSQL;
        return $this->pdo->exec($sql);
    }
    
  public function createTableAdmin() {
        $sql = <<<EOSQL
            CREATE TABLE IF NOT EXISTS admin(
                    id INT NOT NULL AUTO_INCREMENT,
                    user_name VARCHAR(100),
                    user_pass VARCHAR(200),
                    PRIMARY KEY (id)
            );
EOSQL;
        return $this->pdo->exec($sql);
    }
}

$obj = new CreateTableSour();
$obj->createTableAdmin();
